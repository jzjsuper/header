use std::io;
use std::io::prelude::*;

fn process(line: &str, keys: &mut Vec<u64>, values: &mut Vec<String>) {
    let tab_index = line.find('\t').unwrap();
    let key: u64 = (&line[0..tab_index]).parse().unwrap();
    let value = &line[tab_index + 1 ..];
    if keys.len() < 10 {
        keys.push(key);
        values.push(value.to_string());
    } else {
        let mut replace_index = 12333;
        let mut replace_key = key;
        for i in 0usize..10 {
            if keys[i] < replace_key {
                replace_index = i;
                replace_key = keys[i];
            }
        }
        if replace_index != 12333 {
            keys[replace_index] = key;
            values[replace_index] = value.to_string();
        }
    }

}

fn main() {
    let stdin = io::stdin();
    let mut keys: Vec<u64> = Vec::new();
    let mut values: Vec<String> = Vec::new();
    for line in stdin.lock().lines() {
        process(&line.unwrap(), &mut keys, &mut values);
        // println!("{:?}", keys);
    }
    for i in 0..keys.len() {
        println!("{}\t{}", keys[i], values[i]);
    }
}
